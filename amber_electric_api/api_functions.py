"""
This file includes API access functions (no processing, just api access).

Each function is cached based on reasonable expected usage.

Design Philosophy:

* Only cache semantic functions that generate a URL and make a request
* Use Cache Constants for tuning (VERY_SLOW_TTL etc...).

"""
import logging
from typing import List

from asyncache import cached
from cachetools import TTLCache

from amber_electric_api.api_interface import Site, Interval, IntervalType, ChannelType
from amber_electric_api.request import (
    request_json,
)

logger = logging.getLogger(__name__)

DATE_STR_FORMAT = "%Y-%m-%d"

VERY_SLOW_TTL = 3600
SLOW_TTL = 600
MODERATE_TTL = 60
FAST_TTL = 5

TINY_CACHE = 2
MODERATE_CACHE = 100


# Session Global Variables
_module_site: Site | None = None


async def get_main_site() -> Site:
    global _module_site
    if _module_site is None:
        url = "https://api.amber.com.au/v1/sites"
        site_data = await request_json(url)
        _module_site = Site.from_dict(site_data[0])
    return _module_site


@cached(TTLCache(maxsize=TINY_CACHE, ttl=MODERATE_TTL))
async def list_current_prices() -> List[Interval]:
    """
    Includes current and the next two prices
    """
    site = await get_main_site()
    url = f"https://api.amber.com.au/v1/sites/{site.id}/prices/current"
    params = {
        "resolution": 30,
        "previous": 1,
        "next": 2,
    }
    return [Interval.from_dict(a) for a in await request_json(url, params=params)]


async def list_current_feed_in_price() -> float:
    # noinspection PyCallingNonCallable
    for p in await list_current_prices():
        if (
            p.type == IntervalType.Current.value
            and p.channelType == ChannelType.FeedIn.value
        ):
            return p.perKwh
    raise RuntimeError("Current Feed In Price not Found")


async def list_next_feed_in_prices() -> list[float]:

    # noinspection PyCallingNonCallable
    feed_in_forecasts = [
        p
        for p in await list_current_prices()
        if (
            p.type == IntervalType.Forecast.value
            and p.channelType == ChannelType.FeedIn.value
        )
    ]
    feed_in_forecasts.sort(key=lambda x: x.startTime)
    return [x.perKwh for x in feed_in_forecasts]


__all__ = ["list_current_prices", "list_current_feed_in_price"]
