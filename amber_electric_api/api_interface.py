"""
This file tracks interface entities for easier processing
"""
import enum
from dataclasses import dataclass
from typing import Optional

from dataclasses_json import dataclass_json


class ChannelType(enum.Enum):
    General = "general"
    ControlledLoad = "controlledLoad"
    FeedIn = "feedIn"


class IntervalType(enum.Enum):
    PastActual = "ActualInterval"
    Current = "CurrentInterval"
    Forecast = "ForecastInterval"


@dataclass_json
@dataclass
class Channel:
    identifier: str
    type: str
    tariff: str


@dataclass_json
@dataclass
class Site:
    id: str
    nmi: str
    channels: list[Channel]
    network: str
    status: str  # pending, active, closed
    # activeFrom: Optional[str]  # Y-M-D
    # closedOn: Optional[str]  # Y-M-D


@dataclass_json
@dataclass
class Interval:
    type: str
    duration: int
    spotPerKwh: float  # NEM Price
    perKwh: float  # My Consumer Price
    date: str  # NEM Local Time
    nemTime: str  # NEM Local Time
    startTime: str  # UTC
    endTime: str  # UTC
    renewables: int  # percent
    channelType: int  # general, controlledLoad, feedIn
    # tariffInformation
    spikeStatus: str  # none, potential, spike
    # descriptor
