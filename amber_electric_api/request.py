import logging
import requests.utils

from garys_handy_rest_apis import header, requests

logger = logging.getLogger(__name__)

module_key = "AMBER_ELECTRIC_API_KEY"


def request_header():
    return header.request_header("Authorization", module_key)


@requests.uses_session(module_key, request_header)
async def request_json(url: str, params: dict = None) -> dict:
    return await requests.request_json(module_key, url, params)


@requests.uses_session(module_key, request_header)
async def post_json(url: str, body: dict, params: dict = None) -> dict:
    return await requests.post_json(module_key, url, body, params)


@requests.uses_session(module_key, request_header)
async def put_json(url: str, body: dict, params: dict = None) -> dict:
    return await requests.put_json(module_key, url, body, params)
