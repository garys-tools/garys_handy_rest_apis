"""
This file tracks interface entities for easier processing
"""
import datetime
from dataclasses import dataclass
from typing import List, Optional

from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class User:
    id: int
    login: str
    name: str
    email: str
    avatar_url: str
    beta_user: bool
    time_zone: str
    week_start_day: int
    base_currency_code: str
    always_show_base_currency: bool
    created_at: str
    updated_at: str
    using_multiple_currencies: bool
    last_logged_in_at: str
    last_activity_at: str


@dataclass_json
@dataclass
class Category:
    id: int
    title: str
    colour: Optional[str]
    is_transfer: bool
    is_bill: bool
    refund_behaviour: Optional[str]
    children: List["Category"]
    parent_id: Optional[int]
    roll_up: bool
    created_at: str
    updated_at: str


@dataclass_json
@dataclass
class CategoryRule:
    id: int
    category: Category
    # The payee_matches searches within the payee field only
    payee_matches: str
    created_at: str
    updated_at: str


@dataclass_json
@dataclass
class Institution:
    id: int
    title: str
    currency_code: str
    created_at: str
    updated_at: str


@dataclass_json
@dataclass
class Account:
    id: int
    title: str
    currency_code: str
    created_at: str
    updated_at: str


@dataclass_json
@dataclass
class TransactionAccount:
    id: int
    account_id: int
    name: str
    number: Optional[str]
    type: str
    is_net_worth: bool
    currency_code: str
    current_balance: float
    current_balance_in_base_currency: float
    current_balance_exchange_rate: Optional[float]
    current_balance_date: str
    safe_balance: Optional[float]
    safe_balance_in_base_currency: Optional[float]
    starting_balance: Optional[float]
    starting_balance_date: str
    institution: Institution
    created_at: str
    updated_at: str


@dataclass_json
@dataclass
class Scenario:
    id: int
    account_id: int
    title: str
    description: str
    interest_rate: float
    interest_rate_repeat_id: int
    type: str
    is_net_worth: bool
    minimum_value: Optional[float]
    maximum_value: Optional[float]
    achieve_date: Optional[str]
    starting_balance: float
    starting_balance_date: str
    closing_balance: Optional[float]
    closing_balance_date: Optional[str]
    current_balance: float
    current_balance_in_base_currency: float
    current_balance_exchange_rate: Optional[float]
    safe_balance: Optional[float]
    safe_balance_in_base_currency: Optional[float]
    has_safe_balance_adjustment: bool
    created_at: str
    updated_at: str


@dataclass_json
@dataclass
class Transaction:
    id: int
    payee: str
    original_payee: str
    date: str
    upload_source: str
    category: Optional[Category]
    closing_balance: float
    cheque_number: Optional[str]
    memo: Optional[str]
    amount: float
    amount_in_base_currency: float
    type: str
    is_transfer: Optional[bool]
    needs_review: bool
    status: str
    note: Optional[str]
    labels: List[str]
    transaction_account: TransactionAccount
    created_at: str
    updated_at: str

    def get_date(self) -> datetime.date:
        return datetime.datetime.strptime(self.date, "%Y-%m-%d").date()


@dataclass_json
@dataclass
class BudgetElement:
    start_date: str
    end_date: str
    currency_code: str


@dataclass_json
@dataclass
class Budget:
    category: Category
    is_transfer: bool
    expense: Optional[str]
    income: bool


@dataclass_json
@dataclass
class Event:
    id: int
    category: Category
    scenario: Scenario
    amount: float
    amount_in_base_currency: float
    currency_code: str
    date: str
    colour: Optional[str]
    note: Optional[str]
    repeat_type: str
    repeat_interval: int
    series_id: int
    series_start_id: str
    infinite_series: bool
    is_transfer: bool
