"""
This file includes API access functions (no processing, just api access).

Each function is cached based on reasonable expected usage.

Design Philosophy:

* Only cache semantic functions that generate a URL and make a request
* Use Cache Constants for tuning (VERY_SLOW_TTL etc...).

"""
import datetime
import logging
from typing import List, Dict, Union
from asyncache import cached
from cachetools import TTLCache


from pocketsmith_api.api_interface import (
    User,
    CategoryRule,
    Transaction,
    TransactionAccount,
    Category,
    Budget,
    Event,
)

from pocketsmith_api.request import (
    request_json,
    post_json,
    put_json,
    request_count_json,
)


logger = logging.getLogger(__name__)

DATE_STR_FORMAT = "%Y-%m-%d"

VERY_SLOW_TTL = 3600
SLOW_TTL = 600
MODERATE_TTL = 60
FAST_TTL = 5

TINY_CACHE = 2
MODERATE_CACHE = 100


# Session Global Variables
_module_user: User | None = None


async def get_user() -> User:
    global _module_user
    if _module_user is None:
        url = "https://api.pocketsmith.com/v2/me"
        user_data = await request_json(url)
        _module_user = User.from_dict(user_data)
    return _module_user


def date_to_pocket_smith_date_str(d: datetime.date) -> str:
    return d.strftime(DATE_STR_FORMAT)


@cached(TTLCache(maxsize=TINY_CACHE, ttl=VERY_SLOW_TTL))
async def list_category_rules() -> List[CategoryRule]:
    user = await get_user()
    url = f"https://api.pocketsmith.com/v2/users/{user.id}/category_rules"
    return [CategoryRule.from_dict(a) for a in await request_json(url)]


def _flatten_categories(category_list: List[Category]) -> List[Category]:
    """
    A recursive method to collect child categories
    """
    new_list = []
    for c in category_list:
        new_list.append(c)
        if c.children:
            new_list += _flatten_categories(c.children)
    return new_list


@cached(TTLCache(maxsize=TINY_CACHE, ttl=SLOW_TTL))
async def list_categories() -> List[Category]:
    """
    Note that this API returns categories nested and not in a flat
    structure.
    """
    user = await get_user()
    url = f"https://api.pocketsmith.com/v2/users/{user.id}/categories"
    return _flatten_categories([Category.from_dict(a) for a in await request_json(url)])


@cached(TTLCache(maxsize=MODERATE_CACHE, ttl=SLOW_TTL))
async def list_user_transactions(
    search: str = None,
    start_date: Union[datetime.date, str] = None,
    end_date: Union[datetime.date, str] = None,
    only_uncategorised: bool = None,
    transaction_type: str = None,
    count: int = 30,
) -> List[Transaction]:
    """
    From the docs:
    Return transactions matching a keyword search string. The provided
    string is matched against the transaction amount, account name,
    payee, category title, note, labels, and the date in yyyy-mm-dd
    format
    """
    user = await get_user()
    url = f"https://api.pocketsmith.com/v2/users/{user.id}/transactions"

    # noinspection DuplicatedCode
    if count > 100:
        pages = True
        count = 100
    else:
        pages = False
    params = {"per_page": count}

    if start_date:
        if hasattr(start_date, "strftime"):
            start_date = start_date.strftime("%Y-%m-%d")
        params["start_date"] = start_date
    if end_date:
        if hasattr(end_date, "strftime"):
            end_date = end_date.strftime("%Y-%m-%d")
        params["end_date"] = end_date
    if only_uncategorised:
        params["uncategorised"] = 1 if only_uncategorised else 0
    if transaction_type:
        params["type"] = transaction_type
    if search:
        params["search"] = search

    data = await request_json(url, params=params, get_pages=pages)

    transactions = [Transaction.from_dict(a) for a in data]

    return transactions


@cached(TTLCache(maxsize=MODERATE_CACHE, ttl=MODERATE_TTL))
async def list_transactions_in_transaction_accounts(
    account: TransactionAccount,
    count: int = 10,
    start_date: Union[datetime.date, str] = None,
    end_date: Union[datetime.date, str] = None,
    only_uncategorised: bool = None,
    transaction_type: str = None,
    search: str = None,
) -> List[Transaction]:
    """
    This function assumes transaction order is reverse chronological.

    For dates, it accepts a date object or the string as returned
    to the Transaction class.
    """
    url = (
        f"https://api.pocketsmith.com/v2/transaction_accounts/{account.id}/transactions"
    )

    # noinspection DuplicatedCode
    if count > 100:
        pages = True
        count = 100
    else:
        pages = False
    params = {"per_page": count}

    if start_date:
        if hasattr(start_date, "strftime"):
            start_date = start_date.strftime("%Y-%m-%d")
        params["start_date"] = start_date
    if end_date:
        if hasattr(end_date, "strftime"):
            end_date = end_date.strftime("%Y-%m-%d")
        params["end_date"] = end_date
    if only_uncategorised:
        params["only_uncategorised"] = 1 if only_uncategorised else 0
    if transaction_type:
        params["transaction_type"] = transaction_type
    if search:
        params["search"] = search

    data = await request_json(url, params=params, get_pages=pages)

    transactions = [Transaction.from_dict(a) for a in data]

    return transactions


@cached(TTLCache(maxsize=MODERATE_CACHE, ttl=MODERATE_TTL))
async def count_user_transactions(search: str = None) -> int:
    """
    A version of list user, but focuses on counts only
    """
    user = await get_user()
    url = f"https://api.pocketsmith.com/v2/users/{user.id}/transactions"

    params = {"per_page": 10}
    if search:
        params["search"] = search
    return await request_count_json(url, params=params)


@cached(TTLCache(maxsize=TINY_CACHE, ttl=VERY_SLOW_TTL))
async def list_transaction_accounts_in_user() -> List[TransactionAccount]:
    user = await get_user()
    url = f"https://api.pocketsmith.com/v2/users/{user.id}/transaction_accounts"
    params = {"per_page": 100}
    return [
        TransactionAccount.from_dict(t) for t in await request_json(url, params=params)
    ]


# def list_budget_in_user() -> List[TransactionAccount]:
#     """
#     This seems to only list budget forecast data and not budget configuration
#     :return:
#     """
#     user = get_user()
#     url = f"https://api.pocketsmith.com/v2/users/{user.id}/budget"
#     params = {"per_page": 100}
#     data = _request_json(url, params=params)
#     return None


async def transaction_accounts_in_user_by_name() -> Dict[str, TransactionAccount]:
    # noinspection PyCallingNonCallable
    return {x.name: x for x in await list_transaction_accounts_in_user()}


async def create_transaction_in_transaction_account(
    account: TransactionAccount,
    payee: str,
    amount: float,
    date: datetime.date,
    is_transfer: bool = None,
    labels: List[str] = None,
    category_id: int = None,
    note: str = None,
    memo: str = None,
    cheque_number: str = None,
) -> Transaction:
    url = (
        f"https://api.pocketsmith.com/v2/transaction_accounts/{account.id}/transactions"
    )

    body = {
        "account": account.id,
        "payee": payee,
        "amount": float(amount),
        "date": date.strftime("%Y-%m-%d"),
    }
    if is_transfer is not None:
        body["is_transfer"] = is_transfer
    if labels is not None:
        body["labels"] = ",".join(labels)
    if category_id is not None:
        body["category_id"] = category_id
    if note is not None:
        body["note"] = note
    if memo is not None:
        body["memo"] = memo
    if cheque_number is not None:
        body["cheque_number"] = cheque_number

    response = await post_json(url, body=body)
    return Transaction.from_dict(response)


async def update_transaction_in_transaction_account(
    transaction_id: int,
    labels: List[str] = None,
    memo: str = None,
    cheque_number: str = None,
    payee: str = None,
    amount: float = None,
    date: datetime.date = None,
    is_transfer: bool = None,
    category_id: int = None,
    note: str = None,
) -> Transaction:
    url = f"https://api.pocketsmith.com/v2/transactions/{transaction_id}"
    body = {}
    params = {}
    if labels is not None:
        params["labels"] = ",".join(labels)
    if memo is not None:
        body["memo"] = memo
    if cheque_number is not None:
        body["cheque_number"] = cheque_number
    if payee is not None:
        body["payee"] = payee
    if amount is not None:
        body["amount"] = float(amount)
    if date is not None:
        body["date"] = date.strftime("%Y-%m-%d")
    if is_transfer is not None:
        body["is_transfer"] = is_transfer
    if category_id is not None:
        body["category_id"] = category_id
    if note is not None:
        body["note"] = note
    response = await put_json(url, body=body, params=params)
    return Transaction.from_dict(response)


async def create_category_in_user(
    title: str,
    colour: str = None,
    parent_id: int = None,
) -> Category:
    user = await get_user()
    url = f"https://api.pocketsmith.com/v2/users/{user.id}/categories"

    body = {
        "title": title,
    }
    if colour is not None:
        body["colour"] = colour
    if parent_id is not None:
        body["parent_id"] = parent_id

    response = await post_json(url, body=body)
    return Category.from_dict(response)


async def create_rule_in_category(
    category: Category,
    payee_matches: str,
    apply_to_uncategorised: bool = None,
    apply_to_all: bool = None,
) -> CategoryRule:
    url = f"https://api.pocketsmith.com/v2/categories/{category.id}/category_rules"

    body = {
        "payee_matches": payee_matches,
    }
    if apply_to_uncategorised is not None:
        body["apply_to_uncategorised"] = apply_to_uncategorised
    if apply_to_uncategorised is not None:
        body["apply_to_all"] = apply_to_all

    response = await post_json(url, body=body)
    return CategoryRule.from_dict(response)


# def update_transaction_account(
#     account: TransactionAccount, number: str = None
# ) -> TransactionAccount:
#     url = f"https://api.pocketsmith.com/v2/transaction_accounts/{account.id}"
#
#     body = {}
#     if number:
#         body["number"] = number
#
#     response = _put_json(url, body=body)
#     return TransactionAccount.from_dict(response)


@cached(TTLCache(maxsize=MODERATE_CACHE, ttl=SLOW_TTL))
async def list_budgets(roll_up=False) -> List[Budget]:
    """
    Note that this API returns categories nested and not in a flat
    structure.
    """
    user = await get_user()
    url = f"https://api.pocketsmith.com/v2/users/{user.id}/budget"
    params = {"roll_up": roll_up}
    return [Budget.from_dict(a) for a in await request_json(url, params=params)]


@cached(TTLCache(maxsize=MODERATE_CACHE, ttl=SLOW_TTL))
async def list_events(
    start_date: datetime.date, end_date: datetime.date
) -> List[Event]:
    """
    List all events for the user:


    """
    user = await get_user()
    url = f"https://api.pocketsmith.com/v2/users/{user.id}/events"
    params = {
        "start_date": date_to_pocket_smith_date_str(start_date),
        "end_date": date_to_pocket_smith_date_str(end_date),
    }
    return [Event.from_dict(a) for a in await request_json(url, params=params)]


__all__ = [
    "list_category_rules",
    "list_categories",
    "list_user_transactions",
    "list_transactions_in_transaction_accounts",
    "count_user_transactions",
    "transaction_accounts_in_user_by_name",
    "create_transaction_in_transaction_account",
    "update_transaction_in_transaction_account",
    "create_category_in_user",
    "create_rule_in_category",
    "list_budgets",
    "list_events",
]
