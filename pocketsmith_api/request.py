import logging
import requests.utils as python_requests_utils

from garys_handy_rest_apis import header, requests

logger = logging.getLogger(__name__)

module_key = "POCKETSMITH_API_KEY"


def request_header():
    return header.request_header("X-Developer-Key", module_key)


@requests.uses_session(module_key, request_header)
async def request_json(url: str, params: dict = None, get_pages: bool = True) -> dict:
    async with requests.get_session(module_key).get(url, params=params) as response:
        if response.status >= 400:
            logger.debug("Error response: %s", response)
            raise Exception(response.text)
        json = await response.json()

        if isinstance(json, list):
            while get_pages:
                headers = response.headers
                if "Link" in headers:
                    # Check for pagination
                    pages = python_requests_utils.parse_header_links(
                        headers["Link"].rstrip(">").replace(">,<", ",<")
                    )
                    page_rel = {p["rel"]: p["url"] for p in pages}
                    if "next" in page_rel:
                        logger.debug("Retrieving paginated url %s", page_rel["next"])
                        async with requests.get_session(module_key).get(
                            page_rel["next"]
                        ) as response2:
                            json += await response2.json()
                    else:
                        break
                else:
                    break

    return json


@requests.uses_session(module_key, request_header)
async def post_json(url: str, body: dict, params: dict = None) -> dict:
    return await requests.post_json(module_key, url, body, params)


@requests.uses_session(module_key, request_header)
async def put_json(url: str, body: dict, params: dict = None) -> dict:
    return await requests.put_json(module_key, url, body, params)


@requests.uses_session(module_key, request_header)
async def request_count_json(url: str, params: dict = None) -> int:
    """
    Same as _request_json, but returns object count (to avoid accessing the whole DB)
    """
    async with requests.get_session(module_key).get(url, params=params) as response:
        json = await response.json()
        if isinstance(json, list):
            headers = response.headers
            if "Total" in headers:
                items = headers["Total"]
            else:
                items = len(json)
        else:
            items = 1
        return items
