import datetime
import functools
import logging
import os
from functools import cache
from typing import List, Dict, Union

import pathlib
import aiohttp
import requests.utils


from asyncache import cached
from cachetools import TTLCache


from pocketsmith_api.api_interface import (
    User,
    CategoryRule,
    Transaction,
    TransactionAccount,
    Category,
    Budget,
    Event,
)

logger = logging.getLogger(__name__)


@functools.cache
def load_api_key(module_key: str) -> str:
    """
    Used by each module to retrieve the key file from the environment.
    """
    key_path = pathlib.Path(os.environ[module_key])
    with key_path.open("r") as f:
        return f.read()


@functools.cache
def request_header(header_key_field: str, module_key: str) -> dict:
    return {
        "Content-Type": "application/json",
        "Accept": "application/json",
        header_key_field: load_api_key(module_key),
    }
