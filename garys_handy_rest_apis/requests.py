import logging

import aiohttp

logger = logging.getLogger(__name__)

_module_sessions: dict[str, aiohttp.ClientSession] = {}


def uses_session(module_key: str, header_fx: callable):
    """
    Decorate all functions using the module session to ensure it is initialised
    """

    def decorator(func):
        async def wrapper(*args, **kwargs):
            if module_key not in _module_sessions:
                _module_sessions[module_key] = aiohttp.ClientSession(
                    headers=header_fx()
                )

            return await func(*args, **kwargs)

        return wrapper

    return decorator


def get_session(module_key: str) -> aiohttp.ClientSession:
    global _module_sessions
    if module_key not in _module_sessions:
        raise ValueError(
            f"Module with key {module_key} must first be set with set_session"
        )
    return _module_sessions[module_key]


def close_session(module_key: str):
    global _module_sessions
    if module_key in _module_sessions:
        _module_sessions[module_key].close()
        del _module_sessions[module_key]


async def request_json(module_key: str, url: str, params: dict = None) -> dict:
    async with get_session(module_key).get(url, params=params) as response:
        if response.status >= 400:
            logger.debug("Error response: %s", response)
            raise Exception(response.text)
        json = await response.json()

    return json


async def post_json(module_key: str, url: str, body: dict, params: dict = None) -> dict:
    async with get_session(module_key).post(url, params=params, data=body) as response:
        if response.status >= 400:
            logger.debug(response)
            raise Exception(response.text)
        json = await response.json()
    return json


async def put_json(module_key: str, url: str, body: dict, params: dict = None) -> dict:
    async with get_session(module_key).put(url, params=params, data=body) as response:
        if response.status >= 400:
            logger.debug(response)
            raise Exception(response.text)
        json = await response.json()
    return json
